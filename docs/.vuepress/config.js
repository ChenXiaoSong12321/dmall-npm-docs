const nav = require('./menu/nav');
const sidebar = require('./menu/sidebar');

module.exports = {
  base: '/dmall-npm-docs/',
  dest: 'public',
  title: '多点npm',
  description: '多点内部 npm 生态文档查看',
  themeConfig: {
    docsDir: 'docs',
    docsBranch: 'docs',
    // editLinks: true,
    repo: 'http://gitlab.dmall.com/shisong.chen/dmall-npm-docs',
    selectText: '选择语言',
    label: '简体中文',
    repoLabel: '查看源码',
    // editLinkText: '在 GitHub 上编辑此页',
    nav,
    sidebar,
  },
  plugins: ['@vuepress/back-to-top'],
};
