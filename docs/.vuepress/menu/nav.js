module.exports = [
  {
    text: '工具库',
    link: '/utils/',
  },
  {
    text: '小程序',
    items: [
      { text: '业务层', link: '/xcx/bu/' },
      { text: '基础层', link: '/xcx/base/' },
    ],
  },
  {
    text: '其他',
    link: '/other/',
  },
];
