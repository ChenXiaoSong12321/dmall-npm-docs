const utils = require('./siderbar/utils');
const other = require('./siderbar/other');
const xcx = require('./siderbar/xcx-base');

module.exports = {
  '/utils/': utils,
  '/xcx/base/': xcx,
  '/xcx/bu/': xcx,
  '/other/': other,
};
