---
home: true
heroImage: /static/imgs/logo.png
actionText: 立即开始 →
actionLink: /core/
features:
- title: 集中化
  details: 代码集中化管理，通过对依赖和版本的控制达到代码有序管理的目的，增加复用性。
- title: npm packages
  details: 基于 lerna 的 npm包 管理工具对组件库、核心库和开发工具进行有效管理和扩充，实现快速项目搭建。
- title: 代码积累
  details: 内部代码积累，为后续脚手架、自研多端框架等做积累和铺垫。
footer:  Copyright © 2020-present dmall-front-end
---
