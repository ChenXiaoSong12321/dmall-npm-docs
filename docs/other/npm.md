# npm 使用

文档中命令行皆使用 yarn ，npm 需自行处理

- [Yarn vs npm: Everything You Need to Know](https://www.sitepoint.com/yarn-vs-npm/)
- [Yarn vs npm: Everything You Need to Know (译)](https://juejin.im/post/5c64e06ce51d457fce014370)

## 配置

[内部 npm 库地址](http://npm.dmall.com/)

- 配置 npm dmall 域

```sh
npm config set @dmall:registry http://npm.dmall.com/
```

- 查看是否配置成功

```sh
npm config get @hupo:registry
```

## 编写

## 发布

1. 全局安装 lerna

```sh
yarn global add lerna
```

2. 添加用户

```sh
npm adduser --registry http://npm.dmall.com
```

账户密码需向相关人员询问

3. 发布之前不能有未提交的代码，commit之前会进行 `eslint` 和 `单元测试` ，npm库中的包对代码质量有一定的要求。

## 命名占位空间表

| 空间名 | 说明                      | 地址                                           |
| ------ | ------------------------- | ---------------------------------------------- |
| djua   | @dmall/dmall-js-utils-arr | [工具库-数组操作](../utils/dmall-js-utils-arr) |
| djur   | @dmall/dmall-js-utils-reg | [工具库-正则匹配](../utils/dmall-js-utils-reg) |

## 相关文档查询

- [npm 官网](https://docs.npmjs.com/)
- [lerna官网](https://lerna.js.org/)
- [lerna中文文档](https://github.com/minhuaF/blog/issues/2)
- [lerna管理前端packages的最佳实践](https://juejin.im/post/5a989fb451882555731b88c2)
