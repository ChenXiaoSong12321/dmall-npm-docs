# 工具库-正则匹配

@dmall/dmall-js-utils-reg

一些常用的正则匹配

::: tip
命名占位空间: djur
:::

## 快速开始

```sh
yarn add @dmall/dmall-js-utils-reg -S
```

## 全部引用

```js
import { utils as djur_utils } from '@dmall/dmall-js-utils-reg';
console.log(djur_utils.isPositiveNum(9)) //true
console.log(djur_utils.isPositiveNum(2.2)) //false
```

## 按需使用

### 匹配正整数

```js
import { positiveNumReg as djur_positiveNumReg } from '@dmall/dmall-js-utils-reg';
console.log(djur_positiveNumReg); // /^[1-9]d*$/

import { isPositiveNum as djur_isPositiveNum } from '@dmall/dmall-js-utils-reg';
console.log(djur_isPositiveNum(9)); // true
console.log(djur_isPositiveNum(2.2)); // false
```

### 匹配负整数

```js
import { negativeNumReg as djur_negativeNumReg } from '@dmall/dmall-js-utils-reg';
import { isNegativeNum as djur_isNegativeNum } from '@dmall/dmall-js-utils-reg';

console.log(djur_negativeNumReg);
console.log(djur_isNegativeNum(-9)) //true
console.log(djur_isNegativeNum(2.2)) //false
```

### 匹配整数

```js
import { integerReg as djur_integerReg } from '@dmall/dmall-js-utils-reg';
import { isInteger as djur_isInteger } from '@dmall/dmall-js-utils-reg';

console.log(djur_isInteger(-9)) //true
console.log(djur_isInteger(2.2)) //false
```

### 匹配非负浮点数

```js
import { notNegativeFloatNumReg as djur_notNegativeFloatNumReg } from '@dmall/dmall-js-utils-reg';
import { isNotNegativeFloatNum as djur_isNotNegativeFloatNum } from '@dmall/dmall-js-utils-reg';

console.log(djur_isNotNegativeFloatNum(-9)) //false
console.log(djur_isNotNegativeFloatNum(2.2)) //true
```

### 匹配由 26 个英文字母组成的字符串

```js
import { AZazReg as djur_AZazReg } from '@dmall/dmall-js-utils-reg';
import { isAZaz as djur_isAZaz } from '@dmall/dmall-js-utils-reg';

console.log(djur_isAZaz('122a')) //false
console.log(djur_isAZaz('abc')) //true
```

### 匹配由 26 个英文字母的大写组成的字符串

```js
import { AZReg as djur_AZReg } from '@dmall/dmall-js-utils-reg';
import { isAZ as djur_isAZ } from '@dmall/dmall-js-utils-reg';

console.log(djur_isAZ('Acs')) //false
console.log(djur_isAZ('ABC')) //true
```

### 匹配由 26 个英文字母的小写组成的字符串

```js
import { azReg as djur_azReg } from '@dmall/dmall-js-utils-reg';
import { isaz as djur_isaz } from '@dmall/dmall-js-utils-reg';

console.log(djur_isaz('Acs')) //false
console.log(djur_isaz('abc')) //true
```

### 匹配电子邮件地址

```js
import { isEmailAddress as djur_isEmailAddress } from '@dmall/dmall-js-utils-reg';

console.log(djur_isEmailAddress('Acs')) //false
console.log(djur_isEmailAddress('133@qq.com')) //true
```
