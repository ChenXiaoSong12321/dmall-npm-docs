# 工具库-数组操作

@dmall/dmall-js-utils-arr

一些常用的数组操作

::: tip
命名占位空间: djua
:::

## 快速开始

```sh
yarn add @dmall/dmall-js-utils-arr -S
```

## 全部引用

```js
import { utils as djua_utils } from '@dmall/dmall-js-utils-arr';
console.log(djua_utils.isPositiveNum(9)) //true
console.log(djua_utils.isPositiveNum(2.2)) //false
```

## 按需使用

### arrayMax: 返回数组中的最大值

将Math.max()与扩展运算符 (...) 结合使用以获取数组中的最大值。

```js
import { arrayMax as djua_arrayMax } from '@dmall/dmall-js-utils-arr';

const arr = [1, 2, 3, 5];
console.log(djua_arrayMax(arr)); // 5
```

### arrayMin: 返回数组中的最小值

将Math.min()与扩展运算符 (...) 结合使用以获取数组中的最小值。

```js
import { arrayMin as djua_arrayMin } from '@dmall/dmall-js-utils-arr';

let arr = [1, 2, 3, 5];
console.log(djua_arrayMin(arr)); // 1
```

### chunk: 将数组块划分为指定大小的较小数组

使用Array.from()创建新的数组, 这符合将生成的区块数。使用Array.slice()将新数组的每个元素映射到size长度的区块。如果原始数组不能均匀拆分, 则最终的块将包含剩余的元素。

```js
import { chunk as djua_chunk } from '@dmall/dmall-js-utils-arr';

let arr = [1, 2, 3, 5];
console.log(djua_chunk(arr, 2)); // 0: Array [ 1, 2 ],1: Array [ 3, 5 ],
```

### compact: 从数组中移除 falsey 值

使用Array.filter()筛选出 falsey 值 (false、null、0、""、undefined和NaN).

```js
import { compact as djua_compact } from '@dmall/dmall-js-utils-arr';

let arr = [false, null, 0, '', undefined, NaN, 1];
console.log(djua_compact(arr)); // [ 1 ]
```

### countOccurrences: 计算数组中值的出现次数

使用Array.reduce()在每次遇到数组中的特定值时递增计数器。

```js
import { countOccurrences as djua_countOccurrences } from '@dmall/dmall-js-utils-arr';

let arr = [1, 2, 1, 2, 3, 3, 3, 3];
console.log(djua_countOccurrences(arr, 3));// 4
```

### deepFlatten: 深拼合数组

使用递归。使用Array.concat()与空数组 ([]) 和跨页运算符 (...) 来拼合数组。递归拼合作为数组的每个元素。

```js
import { deepFlatten as djua_deepFlatten } from '@dmall/dmall-js-utils-arr';

let arr = [1, 2, [1, 2, [1, 2, [2, 3]]]];
console.log(djua_deepFlatten(arr)); // [ 1, 2, 1, 2, 1, 2, 2, 3 ]
```

### difference: 返回两个数组之间的差异

从b创建Set, 然后使用Array.filter() on 只保留a b中不包含的值.

```js
import { difference as djua_difference } from '@dmall/dmall-js-utils-arr';

let arr = [1, 2, 3];
const arr2 = [2, 3, 4];
console.log(djua_difference(arr, arr2));// [1]
console.log(djua_difference(arr2, arr));// [4]
```

### distinctValuesOfArray: 返回数组的所有不同值(去重)

使用 ES6 Set和...rest运算符放弃所有重复的值。

```js
import { distinctValuesOfArray as djua_distinctValuesOfArray } from '@dmall/dmall-js-utils-arr';

let arr = [1, 2, 3, 1, 2];
console.log(djua_distinctValuesOfArray(arr)); // [ 1, 2, 3 ]
```

### flatten: 拼合数组

使用Array.reduce()获取数组中的所有元素和concat()以拼合它们

```js
import { flatten as djua_flatten } from '@dmall/dmall-js-utils-arr';

const flatten = (arr) => arr.reduce((a, v) => a.concat(v), []);
```

### initializeArrayWithValues: 初始化并填充具有指定值的数组

使用Array(n)创建所需长度的数组,fill(v)以填充所需的值。可以省略value以使用默认值0

```js
import { initializeArrayWithValues as djua_initializeArrayWithValues } from '@dmall/dmall-js-utils-arr';

const initializeArrayWithValues = (n, value = 0) => Array(n).fill(value);
```

### sample: 返回数组中的随机元素

使用Math.random()生成一个随机数, 将它与length相乘, 并使用数学将其舍入到最接近的整数Math.floor()。此方法也适用于字符串

```js
import { sample as djua_sample } from '@dmall/dmall-js-utils-arr';

const sample = (arr) => arr[Math.floor(Math.random() * arr.length)];
```

### shuffle: 随机数组值的顺序

使用Array.sort()可在比较器中使用Math.random()重新排序元素。

```js
import { shuffle as djua_shuffle } from '@dmall/dmall-js-utils-arr';

const shuffle = (arr) => arr.sort(() => Math.random() - 0.5);
```

### similarity: 返回两个数组中都显示的元素的数组

使用filter()可删除不属于values的值, 使用includes()确定.

```js
import { similarity as djua_similarity } from '@dmall/dmall-js-utils-arr';

const similarity = (arr, values) => arr.filter((v) => values.includes(v));
```
